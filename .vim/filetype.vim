if exists("did_load_filetypes")
	finish
endif
augroup filetypedetect
	au BufNewFile,BufRead *.{md,mdwn,mkd,mkdn,mark*} set filetype=markdown
	au BufNewFile,BufRead *.c            setlocal tabstop=4 shiftwidth=4
	au BufNewFile,BufRead *.cpp          setlocal tabstop=4 shiftwidth=4
	au BufNewFile,BufRead *.h            setlocal tabstop=4 shiftwidth=4
	au BufNewFile,BufRead *.t7           setlocal tabstop=8 shiftwidth=8 filetype=tec7
	au BufNewFile,BufRead *.rb           setlocal tabstop=2 shiftwidth=2 expandtab
	au BufNewFile,BufRead *.html,*.htm   setlocal tabstop=2 shiftwidth=2
	au BufNewFile,BufRead *.hsp,hsptmp,*.as setlocal tabstop=8 shiftwidth=8 fileencoding=shift-jis
augroup END
