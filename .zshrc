export LANG=ja_JP.UTF-8
export PATH=$PATH:/usr/local/bin:/Library/TeX/texbin:$HOME/.cargo/bin
alias vi=vim
alias emacs="open -a textedit"
alias xterm="uxterm"
alias composer="php ~/composer.phar"
alias report='pandoc --latex-engine=xelatex -V mainfont="Source Han Sans JP" -V monofont="Ricty Regular" -S -N -s -V geometry:margin=2.5cm --template=custom -V papersize=a4 -H ~/.pandoc/linebreak'

if ! type "pbcopy" > /dev/null; then
	# for linux only
	alias pbcopy='xsel --clipboard --input'
	alias pbpaste='xsel --clipboard --output'
fi

 
# 色を使用出来るようにする
autoload -Uz colors
colors
 
# emacs 風キーバインドにする
bindkey -e
 
# ヒストリの設定
HISTFILE=~/.zsh_history
HISTSIZE=1000000
SAVEHIST=1000000

HISTTIMEFORMAT="[%Y/%M/%D %H:%M:%S] "
 
# vcs_info
autoload -Uz vcs_info
zstyle ':vcs_info:*' check-for-changes true
zstyle ':vcs_info:*' stagedstr '!'
zstyle ':vcs_info:*' unstagedstr "+"
zstyle ':vcs_info:*' formats '%b%c%u'
zstyle ':vcs_info:*' actionformats '%b%c%u'
precmd () {
    psvar=()
    LANG=en_US.UTF-8 vcs_info
    [[ -n "$vcs_info_msg_0_" ]] && psvar[1]="$vcs_info_msg_0_"
}

# プロンプト
# PROMPT="%~ %# "
PROMPT="
(^w^\`).o(%(?..$fg_bold[red]%?${reset_color}|)$fg[blue]${HOST}${reset_color}|%{${fg[yellow]}%}%~%{${reset_color}%}%1(v/|%F{green}%1v%f/))
%# "
 
 
# 単語の区切り文字を指定する
autoload -Uz select-word-style
select-word-style default
# ここで指定した文字は単語区切りとみなされる
# / も区切りと扱うので、^W でディレクトリ１つ分を削除できる
zstyle ':zle:*' word-chars " /=;@:{},|"
zstyle ':zle:*' word-style unspecified
 
########################################
# 補完
# 補完機能を有効にする
fpath=($(brew --prefix)/share/zsh/site-functions $fpath)
autoload -Uz compinit promptinit
compinit -u
zstyle ':completion::complete:*' use-cache true
zstyle ':completion:*:default' menu select=1

# 履歴からの補完
autoload history-search-end
zle -N history-beginning-search-backward-end history-search-end
zle -N history-beginning-search-forward-end history-search-end
bindkey "^P" history-beginning-search-backward-end
bindkey "^N" history-beginning-search-forward-end

# 補完で小文字でも大文字にマッチさせる
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
 
# ../ の後は今いるディレクトリを補完しない
zstyle ':completion:*' ignore-parents parent pwd ..
 
# sudo の後ろでコマンド名を補完する
zstyle ':completion:*:sudo:*' command-path $PATH
 
# ps コマンドのプロセス名補完
zstyle ':completion:*:processes' command 'ps x -o pid,s,args'

########################################
# 修正
setopt correct_all

########################################
# オプション
# 日本語ファイル名を表示可能にする
setopt print_eight_bit
 
# beep を無効にする
setopt no_beep
 
# フローコントロールを無効にする
setopt no_flow_control
 
# '#' 以降をコメントとして扱う
setopt interactive_comments
 
# ディレクトリ名だけでcdする
setopt auto_cd
 
# cd したら自動的にpushdする
setopt auto_pushd

# 重複したディレクトリを追加しない
setopt pushd_ignore_dups
 
# = の後はパス名として補完する
setopt magic_equal_subst
 
# 同時に起動したzshの間でヒストリを共有する
setopt share_history
 
# 同じコマンドをヒストリに残さない
setopt hist_ignore_all_dups
 
# ヒストリファイルに保存するとき、すでに重複したコマンドがあったら古い方を削除する
setopt hist_save_nodups
 
# スペースから始まるコマンド行はヒストリに残さない
setopt hist_ignore_space
 
# ヒストリに保存するときに余分なスペースを削除する
setopt hist_reduce_blanks
 
# 補完候補が複数あるときに自動的に一覧表示する
setopt auto_menu
 
# 高機能なワイルドカード展開を使用する
setopt extended_glob
 
########################################
# キーバインド
 
# ^R で履歴検索をするときに * でワイルドカードを使用出来るようにする
bindkey '^R' history-incremental-pattern-search-backward
 
########################################
# エイリアス
 
alias la='ls -a'
alias ll='ls -l'
 
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
 
alias mkdir='mkdir -p'
 
# sudo の後のコマンドでエイリアスを有効にする
alias sudo='sudo '
 
# グローバルエイリアス
alias -g L='| less'
alias -g G='| grep'
 
# C で標準出力をクリップボードにコピーする
# mollifier delta blog : http://mollifier.hatenablog.com/entry/20100317/p1
if which pbcopy >/dev/null 2>&1 ; then
    # Mac
    alias -g C='| pbcopy'
elif which xsel >/dev/null 2>&1 ; then
    # Linux
    alias -g C='| xsel --input --clipboard'
elif which putclip >/dev/null 2>&1 ; then
    # Cygwin
    alias -g C='| putclip'
fi

########################################
# OS 別の設定
case ${OSTYPE} in
    darwin*)
        #Mac用の設定
        export CLICOLOR=1
        alias ls='ls -G'
        ;;
    linux*)
        #Linux用の設定
        ;;
esac

WSL=`grep -i microsoft /proc/sys/kernel/osrelease`
if [ $? -eq 0 ]; then
	echo "WSL detected: $WSL"
	export PATH=/mnt/c/bin:$PATH
	export SCREENDIR=$HOME/.screen
	echo "Set SCREENDIR=$SCREENDIR"
	alias sumatrapdf='/mnt/c/Program\ Files/SumatraPDF/SumatraPDF.exe'
fi

if [ -d $HOME/.anyenv/ ]; then
	export PATH=$HOME/.anyenv/bin:$PATH
	eval "$(anyenv init - zsh)"
fi
if [ -d $HOME/.rbenv/ ]; then
	export PATH=$HOME/.rbenv/bin:$HOME/.rbenv/shims:$PATH
	eval "$(rbenv init - zsh)"
fi
if [ -d $HOME/.phpenv/ ]; then
	export PATH=$HOME/.phpenv/bin:$HOME/.phpenv/shims:$PATH
	eval "$(phpenv init - zsh)"
fi
if [ -d $HOME/.composer/ ]; then
	export PATH=$HOME/.composer/vendor/bin:$PATH
fi

if [ -d $HOME/.zsh-nvm/ ]; then
	source ~/.zsh-nvm/zsh-nvm.plugin.zsh
fi


########################################
# tmux

# clipboard
if [ `which reattach-to-user-namespace` ]; then
	alias vim='reattach-to-user-namespace vim'
	alias gvim='reattach-to-user-namespace gvim'
fi

export PATH=$PATH:/usr/texbin:/usr/local/sbin:/usr/local/bin

export LANG=ja_JP.UTF-8
export XMODIFIERS=@im=uim
export GTK_IM_MODULE=uim
