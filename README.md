# dotfiles

## Installation
1. clone this repository into dotfiles
1. cd dotfiles
1. git submodule init to initialize submodules. currently we use Neobundle as a submodule
1. git submodule update to install submodules
1. make symlinks in your preference
